package com.sda.collections.lista.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
            Stwórz listę Integerów. Wykonaj zadania:
                - dodaj do listy 5 elementów ze scannera
                - dodaj do listy 5 elementów losowych
                - wypisz elementy
            Sprawdź działanie aplikacji.
         */

        List<Integer> lista = new ArrayList<>();

        // tworze scanner
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
//            lista.add(scanner.nextInt());
            int tymczasowa_zmienna = scanner.nextInt();
            lista.add(tymczasowa_zmienna);
        }

        // wypisanie listy - testowe
        System.out.println(lista);

        // generowanie liczb z random'a
        Random generator = new Random();
        for (int i = 0; i < 5; i++) {
//            lista.add(generator.nextInt());
            int tymczasowa_zmienna = generator.nextInt();
            // wstawienie w listę
            lista.add(tymczasowa_zmienna);
        }

        // wypisanie listy
        System.out.println(lista);
    }
}
