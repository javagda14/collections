package com.sda.collections.lista.zad5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> lista_studentow = new ArrayList<>(
                Arrays.asList(
                        new Student("Michau", "Nijaki", Student.Plec.MEZCZYZNA, "12345"),
                        new Student("Marcin", "Taki", Student.Plec.MEZCZYZNA, "12346"),
                        new Student("Marian", "Inny", Student.Plec.MEZCZYZNA, "12347"),
                        new Student("Mirek", "Nieznany", Student.Plec.KOBIETA, "1225"),
                        new Student("Jurek", "Znany", Student.Plec.KOBIETA, "12545"),
                        new Student("Arek", "Pijany", Student.Plec.MEZCZYZNA, "12745"),
                        new Student("Darek", "Lezacy", Student.Plec.MEZCZYZNA, "1845")
                )
        );

        // a
        System.out.println(lista_studentow);

        // b
        for (Student student : lista_studentow) {
            System.out.println(student);
        }

        // c
        for (Student student : lista_studentow) {
            if (student.getPlec() == Student.Plec.KOBIETA) {
                System.out.println("Jestem kobieta: " + student);
            }
        }

        // d
        for (int i = 0; i < lista_studentow.size(); i++) {
            if (lista_studentow.get(i).getPlec() == Student.Plec.MEZCZYZNA) {
                System.out.println("Jestem mezczyzna: " + lista_studentow.get(i));
            }
        }

        // e
        for (Student studencik : lista_studentow) {
            System.out.println("Indeks : " + studencik.getIndex());
        }

    }
}
