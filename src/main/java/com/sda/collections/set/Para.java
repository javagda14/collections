package com.sda.collections.set;

import java.util.Objects;

public class Para {
    private int lewy;
    private int prawy;

    public Para(int lewy, int prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Para para = (Para) o;
        return lewy == para.lewy &&
                prawy == para.prawy;
    }

    @Override
    public int hashCode() {

        return Objects.hash(lewy, prawy);
    }

    @Override
    public String toString() {
        return "Para{" +
                "lewy=" + lewy +
                ", prawy=" + prawy +
                '}';
    }
}

