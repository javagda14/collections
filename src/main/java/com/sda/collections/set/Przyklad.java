package com.sda.collections.set;

import java.util.*;

public class Przyklad {
    public static void main(String[] args) {
        // set nie posiada duplikatów
//        HashSet<Integer> integers = new HashSet<>();
        String tekst = "awgawf awkf bakwj fakuw fhaw ";
        String[] literki = tekst.split("");

        System.out.println(Arrays.toString(literki));


        Set<Integer> integers = new HashSet<>(Arrays.asList(10, 5));
        integers.add(1);
        integers.add(2);
        integers.add(3);

        System.out.println(integers);
        integers.remove(1);

        integers.add(1);
        integers.add(1);
        integers.add(1);
        integers.add(1);
        integers.add(1);
        integers.add(1);
        integers.add(1);

        System.out.println(integers);


        Set<Para> pary = new HashSet<>();
        Para obiekt = new Para(2, 2);

        pary.add(new Para(1, 1)); // 1
        pary.add(new Para(1, 1)); // 2
        pary.add(obiekt); // 3
        pary.add(obiekt); // 3
        pary.add(null);

//        System.out.println(pary);

        System.out.println("------- Te same operacje na liście która nie usuwa duplikatów");
        List<Para> paras = new ArrayList<>();
        paras.add(new Para(1, 1));
        paras.add(new Para(1, 1));
        paras.add(obiekt);
        paras.add(obiekt);
        paras.add(null);
        paras.add(null);
        System.out.println(paras);

        // dodanie wszystkich elementów listy do setu
        Set<Para> bez_duplikatow = new HashSet<>(paras);
        System.out.println(bez_duplikatow);

        // dodanie wszystkich elementów setu do listy
        List<Para> dalej_lista = new ArrayList<>(bez_duplikatow);
        System.out.println(dalej_lista);


        // usuwanie z kolekcji integers wszystkich elementow z kolekcji stworzonej przez
        // arrays as list - czyli wszystkich elementów na liście [10, 12]
        integers.removeAll(Arrays.asList(10, 12));


        integers.remove(10);
    }
}
